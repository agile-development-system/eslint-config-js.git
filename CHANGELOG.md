## [1.0.8](https://gitee.com/agile-development-system/eslint-config-js/compare/8e8ec25923fd3641efbc7c114df34c60aa6b1457...v1.0.8) (2022-06-14)


### Bug Fixes

* 适配没有babel的项目 ([f7f5ce5](https://gitee.com/agile-development-system/eslint-config-js/commits/f7f5ce59094bd9a503f432c8078d74ead35011b9))


### Features

* 对docs和test目录下的文件忽略一些不适合放在文档中的校验规则 ([73bbe2a](https://gitee.com/agile-development-system/eslint-config-js/commits/73bbe2af0277c4c87e925731446155d20f4fac6b))
* 将没有那么通用的jsdoc规则移除到@agds/eslint-config-jsdoc ([7e159c3](https://gitee.com/agile-development-system/eslint-config-js/commits/7e159c3ecfa0e4f74d3ff0461bdfd1af3f348cae))
* 新增markdown代码块检测 ([7a42b77](https://gitee.com/agile-development-system/eslint-config-js/commits/7a42b77bcdaacd7ce57d45591910f0c26c0443de))
* **index:** 新增json配置 ([2353765](https://gitee.com/agile-development-system/eslint-config-js/commits/2353765fedc8c46601db87d1e435515f384e0e63))
* init ([8e8ec25](https://gitee.com/agile-development-system/eslint-config-js/commits/8e8ec25923fd3641efbc7c114df34c60aa6b1457))



